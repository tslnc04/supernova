package supernova

// Pre-packaged LexerFunc generators

// CreateMatchRuneFunc uses the provided rune to create a LexerFunc for
// lexining that rune
func CreateMatchRuneFunc(r rune) LexerFunc {
	return func(runes []rune) int {
		if runes[0] == r {
			return 1
		}
		return 0
	}
}

// CreateMatchRunesFunc uses the provided runes to create a LexerFunc for
// lexining those runes
func CreateMatchRunesFunc(rs []rune) LexerFunc {
	return func(runes []rune) int {
		if string(runes[:len(rs)]) == string(rs) {
			return len(rs)
		}
		return 0
	}
}

// CreateMatchWhileFunc creates a function that matches the runes as
// long as the given function returns true
func CreateMatchWhileFunc(whileFunc func(r rune) bool) LexerFunc {
	return func(runes []rune) int {
		acc := 0
		for ; acc < len(runes) && whileFunc(runes[acc]); acc++ {
		}
		return acc
	}
}

// CreateMatchEnclosedFunc returns a new LexerFunc that matches
// runes sandwiched between the start and end rune sequences, inclusive
func CreateMatchEnclosedFunc(start, end []rune) LexerFunc {
	return func(runes []rune) int {
		if string(runes[:len(start)]) != string(start) {
			return 0
		}
		// so this maybe kinda sorta panics if the start happens but
		// end does not. be careful
		acc := len(start)
		for string(runes[acc:acc+len(end)]) != string(end) {
			acc++
		}
		return acc + len(end)
	}
}

// CreateMatchToEOLFunc returns a new LexerFunc that matches from the
// start runes until the line feed character, excluding the line feed
// character. However, this will return the carriage return as part of
// the token if the input is encoded with \r\n endings.
func CreateMatchToEOLFunc(start []rune) LexerFunc {
	return func(runes []rune) int {
		if string(runes[:len(start)]) != string(start) {
			return 0
		}
		acc := len(start)
		for runes[acc] != '\n' {
			acc++
		}
		return acc
	}
}
