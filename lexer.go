package supernova

import "sort"

// eof is a constant separate from EOF that is used internally to
// represent the end of input in rune form
const eof = rune(-1)

// EmptyRune is an invalid unicode value used to represent what an empty
// rune would be if it existed. This is used for TokenTypes that have
// variable lengths, so the Length is 0 and the StartRune is EmptyRune.
const EmptyRune = rune(-2)

// EOF is a special TokenType returned when the lexer hits the end of
// the input.
var EOF = &TokenType{
	Name:      "EOF",
	MatchFunc: nil,
	Length:    1,
	StartRune: eof,
}

// stateFunc is the function used internally to represent the next state
// to be executed in the main loop.
type stateFunc func() stateFunc

// LexerFunc is an exported function that represents the user-provided
// functions that take the remaining input and return the length of the
// token that is matched.
type LexerFunc func([]rune) int

// Lexer is the self-contained lexer
type Lexer struct {
	// input is the string that the lexer lexes
	input []rune

	// buffer is where runes yet to be emitted are stored
	buffer []rune

	// Tokens is the output of the lexer when run
	Tokens chan Token

	// position in input array, line number, and position within line,
	// respectively
	pos     int
	linenum int
	linepos int

	// filtered is the list of all TokenTypes that should be
	// filtered from the output of the lexer. It is primarily meant as
	// a way to avoid sending comment tokens to the parser.
	filtered []*TokenType

	// table is the predictive table for lexing where the outer map
	// represents the length of the tokens (0 when the token has a
	// variable length) and the inner map represents the list of
	// possible TokenTypes for the given starting rune
	table map[int]map[rune][]*TokenType

	// tableKeys is a list of the keys of the outermost map in the
	// table, updated upon registering a TokenType and kept constantly
	// in descending order, id est the first element is the longest
	// length, with sorting also ocurring upon registration
	tableKeys []int
}

func (l *Lexer) next() rune {
	if l.pos >= len(l.input) {
		return eof
	}

	var r rune
	r = l.input[l.pos]
	l.pos++
	l.linepos++
	l.buffer = append(l.buffer, r)

	if r == '\n' {
		l.linepos = 0
		l.linenum++
	}

	return r
}

func (l *Lexer) ignore() {
	l.buffer = []rune{}
}

// peek returns the next rune without actually moving the position
// forward, bypassing next to avoid the linepos stack.
func (l *Lexer) peek() rune {
	if l.pos >= len(l.input) {
		return eof
	}

	return l.input[l.pos]
}

func (l *Lexer) emit(tt *TokenType) {
	// if this token has the same
	for _, filteredTT := range l.filtered {
		if filteredTT.Equals(tt) {
			return
		}
	}

	l.Tokens <- Token{
		Pos:     l.pos,
		LineNum: l.linenum,
		LinePos: l.linepos,
		Type:    tt,
		Value:   string(l.buffer),
	}
}

func (l *Lexer) forward(n int) {
	for i := 0; i < n; i++ {
		l.next()
	}
}

// FilterTypes allows for adding additional TokenTypes to be filtered
// after the Lexer instance has been created.
func (l *Lexer) FilterTypes(tts ...*TokenType) {
	l.filtered = append(l.filtered, tts...)
}

// RegisterTypes allows for the registration of new TokenTypes after the
// Lexer instance has been created.
func (l *Lexer) RegisterTypes(tts ...*TokenType) {
	for _, tt := range tts {
		// initialise the inner map
		if _, ok := l.table[tt.Length]; !ok {
			l.table[tt.Length] = make(map[rune][]*TokenType)
		}

		// either append to that StartRune's slice or create a new one
		// with just that TokenType in it
		if runeMap, ok := l.table[tt.Length][tt.StartRune]; ok {
			l.table[tt.Length][tt.StartRune] = append(runeMap, tt)
		} else {
			l.table[tt.Length][tt.StartRune] = []*TokenType{tt}
		}
	}

	tableKeys := make([]int, 0, len(l.table))
	for k := range l.table {
		tableKeys = append(tableKeys, k)
	}
	sort.Sort(sort.Reverse(sort.IntSlice(tableKeys)))
	l.tableKeys = tableKeys
}

func (l *Lexer) lexMain() stateFunc {
	l.ignore()

	r := l.peek()

	if r == eof {
		l.next()
		l.emit(EOF)

		// lexing will stop when nil is returned
		return nil
	}

	for _, length := range l.tableKeys {
		longestLength := 0
		var longestType *TokenType

		if runeTable, ok := l.table[length][r]; ok {
			longestLength, longestType = l.findLongestMatch(runeTable)
		} else if _, ok = l.table[length][EmptyRune]; ok {
			longestLength, longestType = l.findLongestMatch(l.table[length][EmptyRune])
		} else {
			continue
		}
		if longestLength == 0 {
			continue
		}

		l.forward(longestLength)
		l.emit(longestType)

		return l.lexMain
	}

	l.next()
	return l.lexMain
}

func (l *Lexer) findLongestMatch(tts []*TokenType) (int, *TokenType) {
	longestLength := 0
	var longestType *TokenType

	for _, tt := range tts {
		if ttLength := tt.MatchFunc(l.input[l.pos:]); ttLength > longestLength {
			longestLength = ttLength
			longestType = tt
		}
	}

	return longestLength, longestType
}

func (l *Lexer) run() {
	for state := l.lexMain; state != nil; {
		state = state()
	}

	close(l.Tokens)
}

// Lex will run the given input through this Lexer, setting up a new
// channel for Tokens and returning the channel.
func (l *Lexer) Lex(input string) chan Token {
	l.input = []rune(input)
	l.Tokens = make(chan Token)

	go l.run()

	return l.Tokens
}

// NewLexer will create a new instance of a Lexer, initialising values
// as necessary and optionally registering TokenTypes.
func NewLexer(filtered []*TokenType, tts ...*TokenType) *Lexer {
	l := &Lexer{
		buffer:   []rune{},
		pos:      0,
		linenum:  0,
		linepos:  0,
		filtered: filtered,
		table:    make(map[int]map[rune][]*TokenType),
	}

	l.RegisterTypes(tts...)

	return l
}
