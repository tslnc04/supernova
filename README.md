# supernova

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/tslnc04/supernova)](https://goreportcard.com/report/gitlab.com/tslnc04/supernova)

Supernova is an infinite lookahead lexer generator written in Go.

## Features

* Not a generator - call right from Go code
* Reusable - a lexer can be made once and run on multiple inputs
* Concurrent - returns a channel for tokens on demand

## Example

```go
import (
        "fmt"
        "gitlab.com/tslnc04/supernova"
)

var (
        tokenComment = &supernova.TokenType{
                Name: "COMMENT",
                MatchFunc: supernova.CreateMatchToEOLFunc([]rune("//")),
                Length: 0,
                StartRune: supernova.EmptyRune
        }
        // more tokens...
)

// filtered, tokens...
lexer := supernova.NewLexer([]supernova.TokenType{tokenComment}, tokens...)
tokenChan := lexer.Lex("// this is a comment") // token gets filtered

fmt.Println(<-tokenChan)
```

## Documentation

Available at [godoc.org](https://godoc.org/gitlab.com/tslnc04/supernova) or [pkg.go.dev](https://pkg.go.dev/gitlab.com/tslnc04/supernova).

## Works in Progress
- [ ] Tests
- [ ] Improved Documentation
- [ ] Versions
- [ ] Benchmarks

## Contributing

Contributions are welcome either as pull requests or issues.

## Credits

Many of the utility functions that the lexer uses internally are based upon [go-toml](https://github.com/pelletier/go-toml) and thus are licensed under the [MIT License](https://github.com/pelletier/go-toml/blob/master/LICENSE). Copyright notices may be found at the previous link or under [`go-toml-license`](https://gitlab.com/tslnc04/supernova/-/blob/master/go-toml-license) in this repository.

The image used for the repository is courtesy of [NASA and the Chandra X-ray Observatory](https://www.nasa.gov/image-feature/the-tycho-supernova-death-of-a-star). It is an image based of the X-ray data of the Tycho supernova.

## License
Copyright 2020 Timothy Laskoski

Licensed under [MIT](https://gitlab.com/tslnc04/supernova/-/blob/master/LICENSE).
