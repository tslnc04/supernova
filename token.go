package supernova

import (
	"fmt"
	"strconv"
)

// TokenType represents a customisable token type that will be emitted
// by the supernova lexer. Equality should be done by comparing names,
// as with the Equals method.
type TokenType struct {
	Name      string
	MatchFunc func([]rune) int
	Length    int
	StartRune rune
}

// TokenType implements the stringer interface by returning the name of
// the TokenTypes.
func (tt TokenType) String() string {
	return tt.Name
}

// Equals is used to compared if two TokenTypes are equal. This is done
// through testing for equality in names, but one should rely on this
// method in case the equality method were to change.
func (tt TokenType) Equals(other *TokenType) bool {
	return tt.Name == other.Name
}

// Token represents a single token emitted by the supernova lexer
type Token struct {
	Pos     int
	LineNum int
	LinePos int
	Type    *TokenType
	Value   string
}

// Int returns the integer representation of the token if possible,
// otherwise it returns the error for why an integer representation
// could not be found.
func (t Token) Int() (int, error) {
	result, err := strconv.Atoi(t.Value)
	if err != nil {
		return 0, err
	}
	return result, nil
}

func (t Token) String() string {
	return fmt.Sprintf(
		"Token(%d::%d:%d, %s, \"%s\")", t.Pos, t.LineNum, t.LinePos,
		t.Type.String(), t.Value,
	)
}
